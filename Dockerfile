FROM amazon/aws-for-fluent-bit:latest
ADD custom.conf /ecs/fluent-bit.conf
ADD parser.conf /ecs/parser.conf
#CMD ["/fluent-bit/bin/fluent-bit", "-c", "/ecs/fluent-bit.conf"]
CMD ["/fluent-bit/bin/fluent-bit", "-e", "/fluent-bit/firehose.so", "-e", "/fluent-bit/cloudwatch.so", "-e", "/fluent-bit/kinesis.so", "-c", "/ecs/fluent-bit.conf"]
